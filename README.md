# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Just a repo to hold Freewheel library and be able to add it as dependency to our [Freewheel adapter](https://bitbucket.org/npaw/freewheel-adapter-ios/src/master/)
* Currently at Freewheel library 6.20

### How do I get set up? ###

To update the libraries just clone and then replace the framework file for the new one, change version on the .podspec file and follow the same steps to update any pod