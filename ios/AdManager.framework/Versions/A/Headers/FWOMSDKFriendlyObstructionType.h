//
//  FWOMSDKFriendlyObstructionType.h
//  AdManager
//
//  Created by Floam, Scott on 6/3/20.
//  Copyright © 2020 FreeWheel Media Inc. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <OMSDK_Freewheeltv_1_3_5/OMIDFriendlyObstructionType.h>
/**
 * List of allowed friendly obstruction purposes.
 */
typedef NS_ENUM(NSUInteger, FWOMSDKFriendlyObstructionType) {
    /**
     * The friendly obstruction relates to interacting with a video (such as play/pause buttons).
     */
    FWOMSDKFriendlyObstructionMediaControls=OMIDFriendlyObstructionMediaControls,
    /**
     * The friendly obstruction relates to closing an ad (such as a close button).
     */
    FWOMSDKFriendlyObstructionCloseAd=OMIDFriendlyObstructionCloseAd,
    /**
     * The friendly obstruction is not visibly obstructing the ad but may seem so due to technical
     * limitations.
     */
    FWOMSDKFriendlyObstructionNotVisible=OMIDFriendlyObstructionNotVisible,
    /**
     * The friendly obstruction is obstructing for any purpose not already described.
     */
    FWOMSDKFriendlyObstructionOther=OMIDFriendlyObstructionOther
};

